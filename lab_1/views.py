from django.shortcuts import render
from datetime import datetime, date

curr_year = int(datetime.now().strftime("%Y"))
mhs_name = 'Adelia Utami'
birth_date = date(2000, 1, 21)
npm = 1706984493
hobby = 'Danusan'
desc = 'Adel merupakan penduduk Planet Bekasi yang selalu mengejar kereta arah Cikarang tiap akhir minggu. Dia sangat irit dan gemar ngemil. Selalu senang untuk berdiam diri di rumah namun sangat ingin untuk main ke luar rumah.'

friend_name = 'Mila Shania'
friend_birth_date = date(1999, 5, 6)
friend_npm = 1706026821
friend_hobby = 'Tidur'
friend_desc = 'Mila adalah anak sulung yang sangat ambis. Dia selalu menyempatkan waktu untuk tidur dan menonton drama Korea di sela-sela kesibukannya.'

institution = 'Universitas Indonesia'
major = 'Sistem Informasi'

def index(request):
    response = {'name': mhs_name, 'fname': friend_name, 'age': calculate_age(birth_date.year), 'fage': calculate_age(friend_birth_date.year), 'npm': npm, 'fnpm': friend_npm, 'institution': institution, 'major': major, 'hobby': hobby, 'fhobby': friend_hobby, 'desc': desc, 'fdesc': friend_desc}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
